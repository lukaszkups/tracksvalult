Template.playlistTemplate.helpers({
	playlists: function(){
		return Playlist.find({});
	}
});

Template.playlistItem.helpers({
	tracksAmount: function(){
		return Track.find({playlist: this._id}).count();
	}
});

Template.playlistTemplate.events({
	'click ul.playlist li': function(e){
		e.preventDefault();
		var id = $(e.target).data('id');

		Session.set('currentPlaylist', id);
		$('ul.playlist > li.active').removeClass('active');
		$(e.target).addClass('active');
		loadPlaylist(id);
	}
});

var loadPlaylist = function(id){
	var tracks = Track.find({playlist: id});
	
	$('ul.tracksList').empty();
	tracks.forEach(function(track){
		$('ul.tracksList').append('<li data-url="' + track.track_url + '" data-source="' + track.source + '"><span class="author">' + track.author + '</span> - <span class="title">' + track.title + '</span></li>');
	});
}