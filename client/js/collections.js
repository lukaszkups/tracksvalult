Playlist = new Mongo.Collection('playlist');
Track = new Mongo.Collection('track');

Deps.autorun(function(){
	Meteor.subscribe('playlist');
	Meteor.subscribe('track');
});