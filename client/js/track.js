Template.tracksTemplate.events({
	'click ul.tracksList > li': function(e){
		e.preventDefault();
		var link = $(e.currentTarget).data('url'),
			source = $(e.currentTarget).data('source');

		console.log('klikniete');
		$('ul.tracksList > li.active').removeClass('active');
		$(e.currentTarget).addClass('active');
		$('.open').removeClass('open');
		initializePlayer(link, source);
	}
});

initializePlayer = function(link, source){
	$('#player').remove();
	$('#playerWrapper').append('<div id="player"></div>');
	switch(source){
		case 'youtube':
			youtubePlayer(link);
			Session.set('mediaSource', 'youtube');
			break;
		case 'soundcloud':
			soundcloudPlayer(link);
			Session.set('mediaSource', 'soundcloud');
	}
	$('#playButton').addClass('playButton').removeClass('pauseButton');
	Session.set('currentTrack', link);
}