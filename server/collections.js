Playlist = new Mongo.Collection('playlist');
Track = new Mongo.Collection('track');

Meteor.publish('playlist', function(){
	return Meteor.playlist.find({}, {fields: {title: true}});
});

Meteor.publish('track', function(){
	return Meteor.track.find({}, {fields: {author: true, title: true, times_played: true, playlist: true, track_url: true, source:true}});
});
