Playlist.remove();
Track.remove();

if(Playlist.find().count() === 0){
	Playlist.insert({title:'Electronic'});
	Playlist.insert({title:'Dubstep'});
	Playlist.insert({title:'Reggae'});	
}

if(Track.find().count() === 0){
	var p1 = Playlist.findOne({title:'Electronic'}),
		p2 = Playlist.findOne({title:'Dubstep'}),
		p3 = Playlist.findOne({title:'Reggae'});

	Track.insert({author: 'test',title: 'short clip', playlist: p1._id, track_url: 'https://www.youtube.com/watch?v=qBNmY7S0BG8', source:'youtube'});
	Track.insert({author: 'Coyote Kisses',title: 'Stay with You', playlist: p1._id, track_url: 'https://www.youtube.com/watch?v=FX0IpNx_Gxg', source:'youtube'});
	Track.insert({author: 'Coyote Kisses',title: 'This is how You know', playlist: p1._id, track_url: 'https://www.youtube.com/watch?v=yALxpVFRmhs', source:'youtube'});
	Track.insert({author: 'Futurecop!',title: 'Atlantis 1997', playlist: p1._id, track_url: 'https://soundcloud.com/futurecop/futurecop-atlantis-1997-feat', source:'soundcloud'});
	Track.insert({author: 'Seven Lions',title: 'Days to Come', playlist: p1._id, track_url: 'https://www.youtube.com/watch?v=4HVjldqWx4w', source:'youtube'});
	Track.insert({author: 'Sonny Moore (Skrillex)',title: 'Mora', playlist: p1._id, track_url: 'https://soundcloud.com/salvadorcuitla/mora', source:'soundcloud'});
	Track.insert({author: 'Skrillex',title: 'With You, friends', playlist: p1._id, track_url: 'https://soundcloud.com/skrillex/with-you-friends-long-drive', source:'soundcloud'});

	Track.insert({author: 'Seven Lions',title: 'Don\'t Leave', playlist: p1._id, track_url: 'https://www.youtube.com/watch?v=SKlbCjNCDn4', source:'youtube'});
	Track.insert({author: 'Skrillex ft. Ellie Goulding',title: 'Summit', playlist: p2._id, track_url: 'https://www.youtube.com/watch?v=-e_3Cg9GZFU', source:'youtube'});
}